import faker from 'faker';
import mysql from 'mysql';
import { error } from 'util';

const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'tlvkfdk1',
  database: 'join_us'
});

// // SELECTING DATA
// const q = 'SELECT COUNT(*) AS total FROM users';

// connection.query(q, (error, results, fields) => {
//   if (error) throw error;
//   console.log(results);
// });

// // INSERTING DATA
// const person = { 
//   email: faker.internet.email(),
//   created_at: faker.date.past() 
// };

// connection.query('INSERT INTO users SET ? ', person, (err, results, fields) => {
//   if (err) throw error;
//   console.log(results);
// });

// // INSERTING multiple users
// const data = [
//   ['blah@gmail.com', '2017-05-01 03:51:37'],
//   ['ugh@gmail.com', '2017-05-01 03:51:37'],
//   ['meh@gmail.com', '2017-05-01 03:51:37']
// ];

// const q = 'INSERT INTO users (email, created_at) VALUES ?';

// connection.query(q, [data], (err, result) => {
//   console.log(err);
//   console.log(result);
// });


// INSERTING multiple users with for loops
const data = [ ];

for (let i = 0; i < 500; i++) {
  data.push([
    faker.internet.email(),
    faker.date.past()
  ]);
}

const q = 'INSERT INTO users (email, created_at) VALUES ?';

connection.query(q, [data], (err, result) => {
  console.log(err);
  console.log(result);
});

connection.end();